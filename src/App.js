import logo from "./logo.svg";
import "./App.css";

import { useState } from "react";

import { FruitList } from "./components/fruitlist";

function App() {
  const [fruits, setFruits] = useState([
    { name: "banana", color: "yellow", price: 2 },
    { name: "cherry", color: "red", price: 3 },
    { name: "strawberry", color: "red", price: 4 },
  ]);

  function filterRedFruits() {
    return setFruits(fruits.filter((item) => item.color === "red"));
  }

  const totalPrice = fruits.reduce((acc, item) => (acc += item.price), 0);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        <span>Total prince {totalPrice}</span>
        <FruitList fruits={fruits} />

        <button onClick={filterRedFruits}>Botao</button>
      </header>
    </div>
  );
}

export default App;
